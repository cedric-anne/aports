# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=gnome-passwordsafe
pkgver=4_rc
pkgrel=0
pkgdesc="A password manager for GNOME"
url="https://gitlab.gnome.org/World/PasswordSafe"
arch="noarch !s390x !mips !mips64" # Limited by py3-keepass
license="GPL-3.0-only"
depends="python3 py3-keepass>=3.2.1 py3-pycryptodomex libhandy py3-libpwquality py3-gobject3"
makedepends="meson gtk+3.0-dev libhandy1-dev libpwquality-dev gobject-introspection-dev"
checkdepends="appstream-glib desktop-file-utils"
subpackages="$pkgname-lang"
source="https://gitlab.gnome.org/World/PasswordSafe/-/archive/${pkgver/_/.}/PasswordSafe-${pkgver/_/.}.tar.gz"
builddir="$srcdir/PasswordSafe-${pkgver/_/.}"

build() {
	abuild-meson . output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="985dced2f0ced5545d8c30af0e36e755073edd0ae30fb905ea6889ff9356caa48f9b7d0e24a365e0a89e087584f613f5cf5b1d5957861c820c6a5533d361701d  PasswordSafe-4.rc.tar.gz"
